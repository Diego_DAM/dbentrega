<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property string $codigo
 * @property string|null $nombre
 * @property string|null $telefono
 * @property string|null $direccion
 * @property string|null $forma_pago
 *
 * @property Componentes $componentes
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'telefono'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['direccion', 'forma_pago'], 'string', 'max' => 50],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'forma_pago' => 'Forma Pago',
        ];
    }

    /**
     * Gets query for [[Componentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponentes()
    {
        return $this->hasOne(Componentes::className(), ['referencia' => 'codigo']);
    }
}
