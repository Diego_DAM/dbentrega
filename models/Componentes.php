<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "componentes".
 *
 * @property string $referencia
 * @property string|null $estante
 * @property int|null $precio
 * @property int|null $stock
 * @property string|null $tipo_ordenador
 *
 * @property Proveedores $referencia0
 * @property Compras[] $compras
 */
class Componentes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'componentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['referencia'], 'required'],
            [['precio', 'stock'], 'integer'],
            [['referencia'], 'string', 'max' => 10],
            [['estante'], 'string', 'max' => 25],
            [['tipo_ordenador'], 'string', 'max' => 50],
            [['referencia'], 'unique'],
            [['referencia'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['referencia' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'referencia' => 'Referencia',
            'estante' => 'Estante',
            'precio' => 'Precio',
            'stock' => 'Stock',
            'tipo_ordenador' => 'Tipo Ordenador',
        ];
    }

    /**
     * Gets query for [[Referencia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReferencia0()
    {
        return $this->hasOne(Proveedores::className(), ['codigo' => 'referencia']);
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::className(), ['recibo' => 'referencia']);
    }
}
