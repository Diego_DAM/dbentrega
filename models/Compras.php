<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $id
 * @property string|null $recibo
 * @property string|null $fecha_compra
 *
 * @property Clientes $recibo0
 * @property Componentes $recibo1
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_compra'], 'safe'],
            [['recibo'], 'string', 'max' => 10],
            [['recibo'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['recibo' => 'codigo']],
            [['recibo'], 'exist', 'skipOnError' => true, 'targetClass' => Componentes::className(), 'targetAttribute' => ['recibo' => 'referencia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recibo' => 'Recibo',
            'fecha_compra' => 'Fecha Compra',
        ];
    }

    /**
     * Gets query for [[Recibo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecibo0()
    {
        return $this->hasOne(Clientes::className(), ['codigo' => 'recibo']);
    }

    /**
     * Gets query for [[Recibo1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecibo1()
    {
        return $this->hasOne(Componentes::className(), ['referencia' => 'recibo']);
    }
}
