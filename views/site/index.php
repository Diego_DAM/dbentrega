<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="w3-top">
  <div class="w3-bar w3-white w3-padding w3-card" style="letter-spacing:4px;">
    <a href="#home" class="w3-bar-item w3-button">Compasa</a>
    <!-- Right-sided navbar links. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="#about" class="w3-bar-item w3-button">PAGINA PRINCIPAL</a>
      <a href="#menu" class="w3-bar-item w3-button">Componentes</a>
    </div>
  </div>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1600px;min-width:500px" id="home">

  <div class="w3-display-bottomleft w3-padding-large w3-opacity">
  </div>
</header>

<!-- Page content -->
<div class="w3-content" style="max-width:1100px">

  <!-- About Section -->
  <div class="w3-row w3-padding-64" id="about">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
    </div>

    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">Tarjeta Grafica</h1><br>
	  
	  <?= Html::img('@web/images/grafica.jpg', ['alt' => 'Tarjeta gráfica']) ?>
     <br>
      <p class="w3-large">Componente para dar potencia al ordenador y hacer que tanto juegos como programas funcionen mas fluidos<span class="w3-tag w3-light-grey"></span> 165€</p>
      <p class="w3-large w3-text-grey w3-hide-medium">Esta tarjeta grafica vale tanto para odenadores de mesa como para portatiles, al ser de las mas actualizadas del mercado y de las mejores valoradas por su calidad/precio hace que sea un producto indespensable para tu ordenador</p>
    </div>
  </div>
  
  <hr>
  
  <!-- Menu Section -->
  <div class="w3-row w3-padding-64" id="menu">
    <div class="w3-col l6 w3-padding-large">
      <h1 class="w3-center">PRODUCTOS DESTACADOS</h1><br>
      <h4></h4>
	  <?= Html::img('@web/images/destacados.jpg', ['alt' => 'Productos destacados']) ?>
     <br>
      <p class="w3-text-grey"></p><br>
    
      <h4>Disco duro</h4>
      <p class="w3-text-grey">Para tener espacio extra y asi almacenar mas datos 60€</p><br>
    
      <h4>Memoria RAM</h4>
      <p class="w3-text-grey">Permite a tu ordenador tener mas espacio en la memoria y asi poder abrir mas pestañas del Chrome 40€</p><br>
    
      <h4>Caja/Torre</h4>
      <p class="w3-text-grey">Una carcasa chula para un ordenador chulo 37€</p><br>
    
      <h4>Leds</h4>
      <p class="w3-text-grey">Lucecitas de color porque molan 13€</p>    
    </div>
    
    <div class="w3-col l6 w3-padding-large">

    </div>
  </div>

  <hr>

 
