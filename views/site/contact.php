<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Contact Section -->
  <div class="w3-container w3-padding-64" id="contact">
    <h1>CONTACTO</h1><br>
    <p>En caso de no recibir tu pedido a tiempo contacta con nosotros y lo solucionaremos</p>
    <p class="w3-text-blue-grey w3-large"><b>Calle Inventiba/bajoizquierda</b></p>
    <p>Rellene el siguiente formilario</p>
    <form action="/action_page.php" target="_blank">
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Name" required name="Name"></p>
      <p><input class="w3-input w3-padding-16" type="number" placeholder="How many people" required name="People"></p>
      <p><input class="w3-input w3-padding-16" type="datetime-local" placeholder="Date and time" required name="date" value="2017-11-16T20:00"></p>
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Message \ Special requirements" required name="Message"></p>
      <p><button class="w3-button w3-light-grey w3-section" type="submit">ENVIAR MENSAJE</button></p>
    </form>
  </div>
  
<!-- End page content -->
</div>
