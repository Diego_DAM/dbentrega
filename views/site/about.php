<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Acerca';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Compasa es el proyecto que ha sido desarrollado para la realizacion de las prácticas del 2º año de Desarrollo multiplataforma
    </p>
	<p>
        Agradecimientos en especial Maria, Lucia y Guti.
    </p>
</div>
