<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\componentes */

$this->title = 'Update Componentes: ' . $model->referencia;
$this->params['breadcrumbs'][] = ['label' => 'Componentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->referencia, 'url' => ['view', 'id' => $model->referencia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="componentes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
